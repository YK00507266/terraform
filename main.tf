resource "azurerm_resource_group" "demo_rg" {
  name     = "${var.resource_prefix}-RG"
  location = var.node_location
}

# Create a virtual network within the resource group
resource "azurerm_virtual_network" "demo_vnet" {
  name                = "${var.resource_prefix}-vnet"
  resource_group_name = azurerm_resource_group.demo_rg.name
  location            = var.node_location
  address_space       = ["1.0.0.0/16"]
}

# Create a subnets within the virtual network
resource "azurerm_subnet" "demo_subnet" {
  name                 = "${var.resource_prefix}-subnet"
  resource_group_name  = azurerm_resource_group.demo_rg.name
  virtual_network_name = azurerm_virtual_network.demo_vnet.name
  address_prefixes     = ["1.0.0.0/24"]
}

# Create Linux Public IP
resource "azurerm_public_ip" "demo_public_ip" {
  count = var.node_count
  name  = "${var.resource_prefix}-${format("%02d", count.index)}-PublicIP"
  #name = “${var.resource_prefix}-PublicIP”
  location            = azurerm_resource_group.demo_rg.location
  resource_group_name = azurerm_resource_group.demo_rg.name
  allocation_method   = var.Environment == "Test" ? "Static" : "Dynamic"

  tags = {
    environment = "Test"
  }
}

# Create Network Interface
resource "azurerm_network_interface" "demo_nic" {
  count = var.node_count
  #name = “${var.resource_prefix}-NIC”
  name                = "${var.resource_prefix}-${format("%02d", count.index)}-NIC"
  location            = azurerm_resource_group.demo_rg.location
  resource_group_name = azurerm_resource_group.demo_rg.name
  #

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.demo_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = element(azurerm_public_ip.demo_public_ip.*.id, count.index)

    #public_ip_address_id = azurerm_public_ip.example_public_ip.id
    #public_ip_address_id = azurerm_public_ip.example_public_ip.id
  }
}

# Creating resource NSG
resource "azurerm_network_security_group" "demo_nsg" {

  name                = "${var.resource_prefix}-NSG"
  location            = azurerm_resource_group.demo_rg.location
  resource_group_name = azurerm_resource_group.demo_rg.name

  # Security rule can also be defined with resource azurerm_network_security_rule, here just defining it inline.
  security_rule {
    name                       = "Allow Http"
    priority                   = 200
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"

  }
  tags = {
    environment = "Test"
  }
}

# Subnet and NSG association
resource "azurerm_subnet_network_security_group_association" "demo_subnet_nsg_association" {
  subnet_id                 = azurerm_subnet.demo_subnet.id
  network_security_group_id = azurerm_network_security_group.demo_nsg.id

}

# Virtual Machine Creation — Linux
resource "azurerm_virtual_machine" "demo_linux_vm" {
  count = var.node_count
  name  = "${var.resource_prefix}-${format("%02d", count.index)}"
  #name = “${var.resource_prefix}-VM”
  location                      = azurerm_resource_group.demo_rg.location
  resource_group_name           = azurerm_resource_group.demo_rg.name
  network_interface_ids         = [element(azurerm_network_interface.demo_nic.*.id, count.index)]
  vm_size                       = "Standard_B1ms"
  delete_os_disk_on_termination = true


  storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.5"
    version   = "latest"
  }
  storage_os_disk {
    name              = "myosdisk-${count.index}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "linuxhost"
    admin_username = "Demo"
    admin_password = "Password@1234"
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }

  tags = {
    environment = "Test"
  }
}

resource "azurerm_public_ip" "public_ip" {
  name                = "public-ip"
  location            = azurerm_resource_group.demo_rg.location
  resource_group_name = azurerm_resource_group.demo_rg.name
  allocation_method   = "Static"
  sku                 = "Standard"
}


resource "azurerm_lb" "demo_balancer" {
  name                = "demo-balancer"
  location            = azurerm_resource_group.demo_rg.location
  resource_group_name = azurerm_resource_group.demo_rg.name
  sku                 = "Standard"
  sku_tier            = "Regional"
  frontend_ip_configuration {
    name                 = "frontend-ip"
    public_ip_address_id = azurerm_public_ip.public_ip.id
  }

  depends_on = [
    azurerm_public_ip.public_ip
  ]
}

resource "azurerm_lb_backend_address_pool" "PoolA" {
  loadbalancer_id = azurerm_lb.demo_balancer.id
  name            = "PoolA"
  depends_on = [
    azurerm_lb.demo_balancer
  ]
}

resource "azurerm_lb_backend_address_pool_address" "demo_address" {
  count                   = var.node_count
  name                    = "${var.resource_prefix}-${format("%02d", count.index)}-backend-address"
  backend_address_pool_id = azurerm_lb_backend_address_pool.PoolA.id
  virtual_network_id      = azurerm_virtual_network.demo_vnet.id
  ip_address              = azurerm_network_interface.demo_nic[count.index].private_ip_address
  depends_on = [
    azurerm_lb_backend_address_pool.PoolA
  ]
}

resource "azurerm_lb_probe" "ProbeA" {
  resource_group_name = azurerm_resource_group.demo_rg.name
  loadbalancer_id     = azurerm_lb.demo_balancer.id
  name                = "probeA"
  port                = 80
  protocol            = "Tcp"
  depends_on = [
    azurerm_lb.demo_balancer
  ]
}

resource "azurerm_lb_rule" "RuleA" {
  resource_group_name            = azurerm_resource_group.demo_rg.name
  loadbalancer_id                = azurerm_lb.demo_balancer.id
  name                           = "RuleA"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "frontend-ip"
  backend_address_pool_ids       = [azurerm_lb_backend_address_pool.PoolA.id]
  depends_on = [
    azurerm_lb.demo_balancer
  ]
}
