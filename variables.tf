variable "node_location" {
  default = "North Europe"
}

variable "resource_prefix" {
  default = "test1"
}

#variable for Environment
variable "Environment" {
  default = "Test"
}

variable "node_count" {
  default = 1
}
